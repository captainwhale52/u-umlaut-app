import React from 'react';
import { View, WebView, StatusBar, Platform } from 'react-native';
import KeyboardSpacer from 'react-native-keyboard-spacer';

Expo.ScreenOrientation.allow(Expo.ScreenOrientation.Orientation.PORTRAIT_UP);

export default class App extends React.Component {
    render() {
        return (
            <View style={[{flex: 1}]}>
                <StatusBar
                    hidden={true}
                />
                <WebView
                    source={{ uri: 'http://104.155.153.57/' }}  // TODO-Karl: Change based on your app address.
                    style={{marginTop: 0}}
                />
                <KeyboardSpacer/>
            </View>

        );
    }
}