# Ü (U-umlaut) App

## Overview of the app
Ü is a mobile application that we (Karl, Chloe, Fei, Jing, Celine, Hank, and Eason) designed for couples who are in long-distance relationship to create their memory gallery by recording their treasure moments with Ü because when they are apart, it is difficult for them to experience life together. Our idea is to be as a platform for them to make them feel connected with each other even when they are apart from each other due to various reasons such as studying abroad, going on a business trip and etc. Through Ü,  they are going to create their own personalized storybooks, which they are able to share the missing moments together and provide a feeling of presence and connection.

## About this repository
This repository contains a code for running Expo XDE client. This repository is managed by Karl Kim. If you want an access for the latest Bitbucket repository `https://captainwhale52@bitbucket.org/captainwhale52/u-umlaut-app.git`, please send an email to `captainwhale52@gmail.com`.

## Requirement
- Node >= 8.8.1

## Installation
- Go to `https://expo.io/tools` and download / install Expo XDE.
- Open the Expo XDE and click the `Open existing project.`
- Select the {project_root} directory.
- Once the project opened, click the 'Share' button and it will pop up the QR code that you can scan.
- (To test on your phone) Go to Google Play or App Store to download Expo Client app.
- Open the Expo Client App and scan the QR code to load & launch the Ü app.

## Publish
- I (Karl Kim) haven't exported as an actual Android or iOS app. So please check `https://docs.expo.io/versions/latest/guides/publishing.html` for the detailed information for publishing.